"""
    ESERCIZI SUI DIZIONARI PARTE 1- LETTURA ED ESPLORAZIONE DEI DIZIONARI
    by Leo

    Via via che rispondi alle domande, esegui il file.

    In fondo trovi qualche link utile.

    Se hai dei dubbi sul COSA si deve fare chiedi pure, potrei essere stato
    ambiguo senza volerlo.
"""









print("\n\n** ESERCIZIO 1\n")
"""
1.
    1a. Importa in questo file la variabile (di tipo dizionario) 'wc_dict' che
        contiene i dati delle partire della World Cup. La variabile è definita
        all'interno del file (o modulo) 'wc_dict.py'.
    1b. Stampare sulla console il tipo della variabile (utilizzare il metodo type())
"""
# scrivi qua il tuo codice
import wc_dict
wc_dict
type(wc_dict)
print(type(wc_dict))



print("\n\n** ESERCIZIO 2\n")
"""
2. Ispezionare manualmente il dizionario aprendo il file nel quale esso è
   contenuto ('wc_dict.py'). Rispondere alle seguenti domande:
   2a. Da quante chiavi è composto il dizionario 'wc_dict'?
   2b. Qual'è il nome delle chiavi?
   2c. Qual'è il tipo Python del valore associato alle chiavi?
"""
# scrivi qua la tua risposta
print(2)

print('competition')
print('matches')

print('dict')
print('list')



print("\n\n** ESERCIZIO 3\n")
"""
3. Rispondere alle domande 2a, 2b e 2c ma questa volta non ispezionando manualmente il
   dizionario: si deve utilizzare il metodo keys() ed il metodo type() e stampare
   sulla console le risposte alle domande 2a, 2b e 2c con il metodo print().
"""
# scrivi qua il tuo codice
# print(help(wc_dict))
print(type(wc_dict))
print(len(wc_dict))
print(wc_dict.keys())








print("\n\n** ESERCIZIO 4\n")
"""
4. Stampare sulla console il valore del campo "competition".
"""
# scrivi qua il tuo codice
print(wc_dict["competition"])
print(wc_dict.get("competition"))




"""
5. Ottenere il numero delle partite presenti all'interno del dizionario. Si deve
   utilizzare il metodo len().
"""
# scrivi qua il tuo codice
print(wc_dict["matches"])
print(wc_dict.get("matches"))

# print(len(['x', 'y']))
# 2
print(len(wc_dict["matches"]))
# oppure
print(len([
    {
      "id": 200000,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-14T15:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group A",
      "lastUpdated": "2019-01-11T17:30:00Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 5,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 2,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 808,
        "name": "Russia"
      },
      "awayTeam": {
        "id": 801,
        "name": "Saudi Arabia"
      },
      "referees": [
        {
          "id": 49485,
          "name": "Néstor Pitana",
          "nationality": None
        },
        {
          "id": 49486,
          "name": "Juan Pablo Belatti",
          "nationality": None
        },
        {
          "id": 49487,
          "name": "Hernán Maidana",
          "nationality": None
        },
        {
          "id": 49488,
          "name": "Sandro Ricci",
          "nationality": None
        },
        {
          "id": 11448,
          "name": "Emerson Augusto De Carvalho",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        }
      ]
    },
    {
      "id": 200001,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-15T12:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group A",
      "lastUpdated": "2019-01-11T17:30:00Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 825,
        "name": "Egypt"
      },
      "awayTeam": {
        "id": 758,
        "name": "Uruguay"
      },
      "referees": [
        {
          "id": 9558,
          "name": "Björn Kuipers",
          "nationality": None
        },
        {
          "id": 9559,
          "name": "Sander van Roekel",
          "nationality": None
        },
        {
          "id": 9560,
          "name": "Erwin Zeinstra",
          "nationality": None
        },
        {
          "id": 9371,
          "name": "Milorad Mažić",
          "nationality": None
        },
        {
          "id": 9372,
          "name": "Milovan Ristić",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        },
        {
          "id": 43884,
          "name": "Cyril Gringore",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 9374,
          "name": "Clément Turpin",
          "nationality": None
        }
      ]
    },
    {
      "id": 200006,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-15T15:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group B",
      "lastUpdated": "2019-01-11T17:30:00Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 815,
        "name": "Morocco"
      },
      "awayTeam": {
        "id": 840,
        "name": "Iran"
      },
      "referees": [
        {
          "id": 30687,
          "name": "Cüneyt Çakιr",
          "nationality": None
        },
        {
          "id": 30688,
          "name": "Bahattin Duran",
          "nationality": None
        },
        {
          "id": 30689,
          "name": "Tarık Ongun",
          "nationality": None
        },
        {
          "id": 43938,
          "name": "Sergey Karasev",
          "nationality": None
        },
        {
          "id": 43939,
          "name": "Anton Averianov",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 43876,
          "name": "Mark Borsch",
          "nationality": None
        },
        {
          "id": 55667,
          "name": "Jair Marrufo",
          "nationality": None
        }
      ]
    },
    {
      "id": 200007,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-15T18:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group B",
      "lastUpdated": "2019-01-11T17:30:00Z",
      "score": {
        "winner": "DRAW",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 3,
          "awayTeam": 3
        },
        "halfTime": {
          "homeTeam": 2,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 765,
        "name": "Portugal"
      },
      "awayTeam": {
        "id": 760,
        "name": "Spain"
      },
      "referees": [
        {
          "id": 10988,
          "name": "Gianluca Rocchi",
          "nationality": None
        },
        {
          "id": 11056,
          "name": "Elenito Di Liberatore",
          "nationality": None
        },
        {
          "id": 11058,
          "name": "Mauro Tonolini",
          "nationality": None
        },
        {
          "id": 49525,
          "name": "Ryūji Satō",
          "nationality": None
        },
        {
          "id": 55658,
          "name": "Toru Sagara",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        }
      ]
    },
    {
      "id": 200012,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-16T10:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group C",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 773,
        "name": "France"
      },
      "awayTeam": {
        "id": 779,
        "name": "Australia"
      },
      "referees": [
        {
          "id": 29474,
          "name": "Andrés Cunha",
          "nationality": None
        },
        {
          "id": 28776,
          "name": "Nicolás Tarán",
          "nationality": None
        },
        {
          "id": 28832,
          "name": "Mauricio Espinosa",
          "nationality": None
        },
        {
          "id": 23895,
          "name": "Julio Bascuñán",
          "nationality": None
        },
        {
          "id": 55659,
          "name": "Christian Schiemann",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 49487,
          "name": "Hernán Maidana",
          "nationality": None
        },
        {
          "id": 15713,
          "name": "Tiago Martins",
          "nationality": None
        },
        {
          "id": 55667,
          "name": "Jair Marrufo",
          "nationality": None
        }
      ]
    },
    {
      "id": 200018,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-16T13:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group D",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "DRAW",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 762,
        "name": "Argentina"
      },
      "awayTeam": {
        "id": 1066,
        "name": "Iceland"
      },
      "referees": [
        {
          "id": 43888,
          "name": "Szymon Marciniak",
          "nationality": None
        },
        {
          "id": 43889,
          "name": "Paweł Sokolnicki",
          "nationality": None
        },
        {
          "id": 43890,
          "name": "Tomasz Listkiewicz",
          "nationality": None
        },
        {
          "id": 23062,
          "name": "Wilmar Roldán",
          "nationality": None
        },
        {
          "id": 23046,
          "name": "Alexander Guzmán",
          "nationality": None
        },
        {
          "id": 55724,
          "name": "Mark Geiger",
          "nationality": None
        },
        {
          "id": 55725,
          "name": "Joe Fletcher",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 18127,
          "name": "Gery Vargas",
          "nationality": None
        }
      ]
    },
    {
      "id": 200013,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-16T16:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group C",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 832,
        "name": "Peru"
      },
      "awayTeam": {
        "id": 782,
        "name": "Denmark"
      },
      "referees": [
        {
          "id": 55660,
          "name": "Bakary Papa Gassama",
          "nationality": None
        },
        {
          "id": 55661,
          "name": "Jean-Claude Birumushahu",
          "nationality": None
        },
        {
          "id": 55662,
          "name": "Abdelhak Etchiali",
          "nationality": None
        },
        {
          "id": 55663,
          "name": "Mehdi Abid Charef",
          "nationality": None
        },
        {
          "id": 55664,
          "name": "Anouar Hmila",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 43876,
          "name": "Mark Borsch",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        }
      ]
    },
    {
      "id": 200019,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-16T19:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group D",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 799,
        "name": "Croatia"
      },
      "awayTeam": {
        "id": 776,
        "name": "Nigeria"
      },
      "referees": [
        {
          "id": 49488,
          "name": "Sandro Ricci",
          "nationality": None
        },
        {
          "id": 11448,
          "name": "Emerson Augusto De Carvalho",
          "nationality": None
        },
        {
          "id": 11468,
          "name": "Marcelo Carvalho Van Gasse",
          "nationality": None
        },
        {
          "id": 43869,
          "name": "Antonio Matéu",
          "nationality": None
        },
        {
          "id": 134,
          "name": "Pau Cebrián",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        },
        {
          "id": 11412,
          "name": "Wilton Pereira Sampaio",
          "nationality": None
        },
        {
          "id": 49487,
          "name": "Hernán Maidana",
          "nationality": None
        },
        {
          "id": 43540,
          "name": "Artur Soares Dias",
          "nationality": None
        }
      ]
    },
    {
      "id": 200024,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-17T12:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group E",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 793,
        "name": "Costa Rica"
      },
      "awayTeam": {
        "id": 780,
        "name": "Serbia"
      },
      "referees": [
        {
          "id": 55961,
          "name": "Malang Diédhiou",
          "nationality": None
        },
        {
          "id": 55962,
          "name": "Djibril Camara",
          "nationality": None
        },
        {
          "id": 55963,
          "name": "El Hadji Malick Samba",
          "nationality": None
        },
        {
          "id": 55964,
          "name": "Bamlak Tessema",
          "nationality": None
        },
        {
          "id": 43940,
          "name": "Tikhon Kalugin",
          "nationality": None
        },
        {
          "id": 9374,
          "name": "Clément Turpin",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 43884,
          "name": "Cyril Gringore",
          "nationality": None
        },
        {
          "id": 43540,
          "name": "Artur Soares Dias",
          "nationality": None
        }
      ]
    },
    {
      "id": 200030,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-17T15:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group F",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 759,
        "name": "Germany"
      },
      "awayTeam": {
        "id": 769,
        "name": "Mexico"
      },
      "referees": [
        {
          "id": 55970,
          "name": "Alireza Faghani",
          "nationality": None
        },
        {
          "id": 55971,
          "name": "Reza Sokhandan",
          "nationality": None
        },
        {
          "id": 55972,
          "name": "Mohammadreza Mansouri",
          "nationality": None
        },
        {
          "id": 55973,
          "name": "Mohammed Abdulla Hassan",
          "nationality": None
        },
        {
          "id": 55974,
          "name": "Mohamed Ahmed Al Hammadi",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 11412,
          "name": "Wilton Pereira Sampaio",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 55724,
          "name": "Mark Geiger",
          "nationality": None
        }
      ]
    },
    {
      "id": 200025,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-17T18:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group E",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "DRAW",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 764,
        "name": "Brazil"
      },
      "awayTeam": {
        "id": 788,
        "name": "Switzerland"
      },
      "referees": [
        {
          "id": 56007,
          "name": "César Ramos",
          "nationality": None
        },
        {
          "id": 56008,
          "name": "Marvin Torrentera",
          "nationality": None
        },
        {
          "id": 56009,
          "name": "Miguel Ángel Hernández",
          "nationality": None
        },
        {
          "id": 56010,
          "name": "John Pitti",
          "nationality": None
        },
        {
          "id": 56011,
          "name": "Gabriel Victoria",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 11056,
          "name": "Elenito Di Liberatore",
          "nationality": None
        },
        {
          "id": 10988,
          "name": "Gianluca Rocchi",
          "nationality": None
        }
      ]
    },
    {
      "id": 200031,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-18T12:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group F",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 792,
        "name": "Sweden"
      },
      "awayTeam": {
        "id": 772,
        "name": "Korea Republic"
      },
      "referees": [
        {
          "id": 56016,
          "name": "Joel Aguilar",
          "nationality": None
        },
        {
          "id": 56017,
          "name": "Juan Zumba",
          "nationality": None
        },
        {
          "id": 56018,
          "name": "Juan Carlos Mora",
          "nationality": None
        },
        {
          "id": 56019,
          "name": "Norbert Hauata",
          "nationality": None
        },
        {
          "id": 56020,
          "name": "Bertrand Brial",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 56026,
          "name": "Abdelrahman Al Jassim",
          "nationality": None
        },
        {
          "id": 56027,
          "name": "Taleb Salem Al Marri",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        }
      ]
    },
    {
      "id": 200036,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-18T15:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group G",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 3,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 805,
        "name": "Belgium"
      },
      "awayTeam": {
        "id": 1836,
        "name": "Panama"
      },
      "referees": [
        {
          "id": 56021,
          "name": "Janny Sikazwe",
          "nationality": None
        },
        {
          "id": 56022,
          "name": "Jerson dos Santos",
          "nationality": None
        },
        {
          "id": 33897,
          "name": "Zakhele Siwela",
          "nationality": None
        },
        {
          "id": 49525,
          "name": "Ryūji Satō",
          "nationality": None
        },
        {
          "id": 55658,
          "name": "Toru Sagara",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        },
        {
          "id": 9559,
          "name": "Sander van Roekel",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        }
      ]
    },
    {
      "id": 200037,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-18T18:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group G",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 802,
        "name": "Tunisia"
      },
      "awayTeam": {
        "id": 770,
        "name": "England"
      },
      "referees": [
        {
          "id": 23062,
          "name": "Wilmar Roldán",
          "nationality": None
        },
        {
          "id": 23046,
          "name": "Alexander Guzmán",
          "nationality": None
        },
        {
          "id": 22961,
          "name": "Christian de la Cruz",
          "nationality": None
        },
        {
          "id": 56024,
          "name": "Ricardo Montero",
          "nationality": None
        },
        {
          "id": 56063,
          "name": "Hiroshi Yamauchi",
          "nationality": None
        },
        {
          "id": 49488,
          "name": "Sandro Ricci",
          "nationality": None
        },
        {
          "id": 18127,
          "name": "Gery Vargas",
          "nationality": None
        },
        {
          "id": 11448,
          "name": "Emerson Augusto De Carvalho",
          "nationality": None
        },
        {
          "id": 15713,
          "name": "Tiago Martins",
          "nationality": None
        }
      ]
    },
    {
      "id": 200042,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-19T12:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group H",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 818,
        "name": "Colombia"
      },
      "awayTeam": {
        "id": 766,
        "name": "Japan"
      },
      "referees": [
        {
          "id": 9346,
          "name": "Damir Skomina",
          "nationality": None
        },
        {
          "id": 9347,
          "name": "Jure Praprotnik",
          "nationality": None
        },
        {
          "id": 9348,
          "name": "Robert Vukan",
          "nationality": None
        },
        {
          "id": 55663,
          "name": "Mehdi Abid Charef",
          "nationality": None
        },
        {
          "id": 55664,
          "name": "Anouar Hmila",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 9559,
          "name": "Sander van Roekel",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        }
      ]
    },
    {
      "id": 200043,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-19T15:00:00Z",
      "status": "FINISHED",
      "matchday": 1,
      "stage": "GROUP_STAGE",
      "group": "Group H",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 794,
        "name": "Poland"
      },
      "awayTeam": {
        "id": 804,
        "name": "Senegal"
      },
      "referees": [
        {
          "id": 56071,
          "name": "Nawaf Shukralla",
          "nationality": None
        },
        {
          "id": 56072,
          "name": "Yaser Tulefat",
          "nationality": None
        },
        {
          "id": 56027,
          "name": "Taleb Salem Al Marri",
          "nationality": None
        },
        {
          "id": 56026,
          "name": "Abdelrahman Al Jassim",
          "nationality": None
        },
        {
          "id": 55974,
          "name": "Mohamed Ahmed Al Hammadi",
          "nationality": None
        },
        {
          "id": 43540,
          "name": "Artur Soares Dias",
          "nationality": None
        },
        {
          "id": 15713,
          "name": "Tiago Martins",
          "nationality": None
        },
        {
          "id": 49487,
          "name": "Hernán Maidana",
          "nationality": None
        },
        {
          "id": 11412,
          "name": "Wilton Pereira Sampaio",
          "nationality": None
        }
      ]
    },
    {
      "id": 200002,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-19T18:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group A",
      "lastUpdated": "2019-01-11T17:30:00Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 3,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 808,
        "name": "Russia"
      },
      "awayTeam": {
        "id": 825,
        "name": "Egypt"
      },
      "referees": [
        {
          "id": 56073,
          "name": "Enrique Cáceres",
          "nationality": None
        },
        {
          "id": 56074,
          "name": "Eduardo Cardozo",
          "nationality": None
        },
        {
          "id": 56075,
          "name": "Juan Zorrilla",
          "nationality": None
        },
        {
          "id": 30687,
          "name": "Cüneyt Çakιr",
          "nationality": None
        },
        {
          "id": 30688,
          "name": "Bahattin Duran",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 43888,
          "name": "Szymon Marciniak",
          "nationality": None
        }
      ]
    },
    {
      "id": 200008,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-20T12:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group B",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 765,
        "name": "Portugal"
      },
      "awayTeam": {
        "id": 815,
        "name": "Morocco"
      },
      "referees": [
        {
          "id": 55724,
          "name": "Mark Geiger",
          "nationality": None
        },
        {
          "id": 55725,
          "name": "Joe Fletcher",
          "nationality": None
        },
        {
          "id": 56076,
          "name": "Frank Anderson",
          "nationality": None
        },
        {
          "id": 43938,
          "name": "Sergey Karasev",
          "nationality": None
        },
        {
          "id": 43939,
          "name": "Anton Averianov",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        },
        {
          "id": 55667,
          "name": "Jair Marrufo",
          "nationality": None
        },
        {
          "id": 56085,
          "name": "Simon Lount",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        }
      ]
    },
    {
      "id": 200003,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-20T15:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group A",
      "lastUpdated": "2019-01-11T17:30:00Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 758,
        "name": "Uruguay"
      },
      "awayTeam": {
        "id": 801,
        "name": "Saudi Arabia"
      },
      "referees": [
        {
          "id": 9374,
          "name": "Clément Turpin",
          "nationality": None
        },
        {
          "id": 43883,
          "name": "Nicolas Danos",
          "nationality": None
        },
        {
          "id": 43884,
          "name": "Cyril Gringore",
          "nationality": None
        },
        {
          "id": 56010,
          "name": "John Pitti",
          "nationality": None
        },
        {
          "id": 56011,
          "name": "Gabriel Victoria",
          "nationality": None
        },
        {
          "id": 43888,
          "name": "Szymon Marciniak",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 43889,
          "name": "Paweł Sokolnicki",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        }
      ]
    },
    {
      "id": 200009,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-20T18:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group B",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 840,
        "name": "Iran"
      },
      "awayTeam": {
        "id": 760,
        "name": "Spain"
      },
      "referees": [
        {
          "id": 29474,
          "name": "Andrés Cunha",
          "nationality": None
        },
        {
          "id": 28776,
          "name": "Nicolás Tarán",
          "nationality": None
        },
        {
          "id": 28832,
          "name": "Mauricio Espinosa",
          "nationality": None
        },
        {
          "id": 23895,
          "name": "Julio Bascuñán",
          "nationality": None
        },
        {
          "id": 55659,
          "name": "Christian Schiemann",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 11412,
          "name": "Wilton Pereira Sampaio",
          "nationality": None
        },
        {
          "id": 23046,
          "name": "Alexander Guzmán",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        }
      ]
    },
    {
      "id": 200014,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-21T12:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group C",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "DRAW",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 782,
        "name": "Denmark"
      },
      "awayTeam": {
        "id": 779,
        "name": "Australia"
      },
      "referees": [
        {
          "id": 43869,
          "name": "Antonio Matéu",
          "nationality": None
        },
        {
          "id": 134,
          "name": "Pau Cebrián",
          "nationality": None
        },
        {
          "id": 82,
          "name": "Roberto Díaz",
          "nationality": None
        },
        {
          "id": 55964,
          "name": "Bamlak Tessema",
          "nationality": None
        },
        {
          "id": 56018,
          "name": "Juan Carlos Mora",
          "nationality": None
        },
        {
          "id": 55724,
          "name": "Mark Geiger",
          "nationality": None
        },
        {
          "id": 55667,
          "name": "Jair Marrufo",
          "nationality": None
        },
        {
          "id": 55725,
          "name": "Joe Fletcher",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        }
      ]
    },
    {
      "id": 200015,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-21T15:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group C",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 773,
        "name": "France"
      },
      "awayTeam": {
        "id": 832,
        "name": "Peru"
      },
      "referees": [
        {
          "id": 55973,
          "name": "Mohammed Abdulla Hassan",
          "nationality": None
        },
        {
          "id": 55974,
          "name": "Mohamed Ahmed Al Hammadi",
          "nationality": None
        },
        {
          "id": 56084,
          "name": "Hasan Mohamed Al Mahri",
          "nationality": None
        },
        {
          "id": 56021,
          "name": "Janny Sikazwe",
          "nationality": None
        },
        {
          "id": 56022,
          "name": "Jerson dos Santos",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        },
        {
          "id": 56026,
          "name": "Abdelrahman Al Jassim",
          "nationality": None
        },
        {
          "id": 56027,
          "name": "Taleb Salem Al Marri",
          "nationality": None
        },
        {
          "id": 43888,
          "name": "Szymon Marciniak",
          "nationality": None
        }
      ]
    },
    {
      "id": 200020,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-21T18:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group D",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 3
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 762,
        "name": "Argentina"
      },
      "awayTeam": {
        "id": 799,
        "name": "Croatia"
      },
      "referees": [
        {
          "id": 56184,
          "name": "Ravshan Irmatov",
          "nationality": None
        },
        {
          "id": 56185,
          "name": "Abdukhamidullo Rasulov",
          "nationality": None
        },
        {
          "id": 56186,
          "name": "Jakhongir Saidov",
          "nationality": None
        },
        {
          "id": 56019,
          "name": "Norbert Hauata",
          "nationality": None
        },
        {
          "id": 56020,
          "name": "Bertrand Brial",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 56025,
          "name": "Corey Rockwell",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        }
      ]
    },
    {
      "id": 200026,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-22T12:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group E",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 764,
        "name": "Brazil"
      },
      "awayTeam": {
        "id": 793,
        "name": "Costa Rica"
      },
      "referees": [
        {
          "id": 9558,
          "name": "Björn Kuipers",
          "nationality": None
        },
        {
          "id": 9559,
          "name": "Sander van Roekel",
          "nationality": None
        },
        {
          "id": 9560,
          "name": "Erwin Zeinstra",
          "nationality": None
        },
        {
          "id": 9346,
          "name": "Damir Skomina",
          "nationality": None
        },
        {
          "id": 9347,
          "name": "Jure Praprotnik",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        },
        {
          "id": 43540,
          "name": "Artur Soares Dias",
          "nationality": None
        },
        {
          "id": 55725,
          "name": "Joe Fletcher",
          "nationality": None
        },
        {
          "id": 55724,
          "name": "Mark Geiger",
          "nationality": None
        }
      ]
    },
    {
      "id": 200021,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-22T15:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group D",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 776,
        "name": "Nigeria"
      },
      "awayTeam": {
        "id": 1066,
        "name": "Iceland"
      },
      "referees": [
        {
          "id": 56176,
          "name": "Matt Conger",
          "nationality": None
        },
        {
          "id": 56085,
          "name": "Simon Lount",
          "nationality": None
        },
        {
          "id": 56177,
          "name": "Tevita Makasini",
          "nationality": None
        },
        {
          "id": 56024,
          "name": "Ricardo Montero",
          "nationality": None
        },
        {
          "id": 56063,
          "name": "Hiroshi Yamauchi",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 11056,
          "name": "Elenito Di Liberatore",
          "nationality": None
        },
        {
          "id": 10988,
          "name": "Gianluca Rocchi",
          "nationality": None
        }
      ]
    },
    {
      "id": 200027,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-22T18:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group E",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 780,
        "name": "Serbia"
      },
      "awayTeam": {
        "id": 788,
        "name": "Switzerland"
      },
      "referees": [
        {
          "id": 43875,
          "name": "Felix Brych",
          "nationality": None
        },
        {
          "id": 43876,
          "name": "Mark Borsch",
          "nationality": None
        },
        {
          "id": 43877,
          "name": "Stefan Lupp",
          "nationality": None
        },
        {
          "id": 56071,
          "name": "Nawaf Shukralla",
          "nationality": None
        },
        {
          "id": 56072,
          "name": "Yaser Tulefat",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 9374,
          "name": "Clément Turpin",
          "nationality": None
        }
      ]
    },
    {
      "id": 200038,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-23T12:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group G",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 5,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 3,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 805,
        "name": "Belgium"
      },
      "awayTeam": {
        "id": 802,
        "name": "Tunisia"
      },
      "referees": [
        {
          "id": 55667,
          "name": "Jair Marrufo",
          "nationality": None
        },
        {
          "id": 56025,
          "name": "Corey Rockwell",
          "nationality": None
        },
        {
          "id": 56017,
          "name": "Juan Zumba",
          "nationality": None
        },
        {
          "id": 29474,
          "name": "Andrés Cunha",
          "nationality": None
        },
        {
          "id": 28776,
          "name": "Nicolás Tarán",
          "nationality": None
        },
        {
          "id": 55724,
          "name": "Mark Geiger",
          "nationality": None
        },
        {
          "id": 55725,
          "name": "Joe Fletcher",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        }
      ]
    },
    {
      "id": 200032,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-23T15:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group F",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 772,
        "name": "Korea Republic"
      },
      "awayTeam": {
        "id": 769,
        "name": "Mexico"
      },
      "referees": [
        {
          "id": 9371,
          "name": "Milorad Mažić",
          "nationality": None
        },
        {
          "id": 9372,
          "name": "Milovan Ristić",
          "nationality": None
        },
        {
          "id": 56231,
          "name": "Dalibor Đurđević",
          "nationality": None
        },
        {
          "id": 56010,
          "name": "John Pitti",
          "nationality": None
        },
        {
          "id": 56011,
          "name": "Gabriel Victoria",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 43540,
          "name": "Artur Soares Dias",
          "nationality": None
        },
        {
          "id": 15713,
          "name": "Tiago Martins",
          "nationality": None
        }
      ]
    },
    {
      "id": 200033,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-23T18:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group F",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 759,
        "name": "Germany"
      },
      "awayTeam": {
        "id": 792,
        "name": "Sweden"
      },
      "referees": [
        {
          "id": 43888,
          "name": "Szymon Marciniak",
          "nationality": None
        },
        {
          "id": 43889,
          "name": "Paweł Sokolnicki",
          "nationality": None
        },
        {
          "id": 43890,
          "name": "Tomasz Listkiewicz",
          "nationality": None
        },
        {
          "id": 49525,
          "name": "Ryūji Satō",
          "nationality": None
        },
        {
          "id": 55658,
          "name": "Toru Sagara",
          "nationality": None
        },
        {
          "id": 9374,
          "name": "Clément Turpin",
          "nationality": None
        },
        {
          "id": 43884,
          "name": "Cyril Gringore",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        }
      ]
    },
    {
      "id": 200039,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-24T12:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group G",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 6,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 5,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 770,
        "name": "England"
      },
      "awayTeam": {
        "id": 1836,
        "name": "Panama"
      },
      "referees": [
        {
          "id": 56393,
          "name": "Gehad Grisha",
          "nationality": None
        },
        {
          "id": 56394,
          "name": "Redouane Achik",
          "nationality": None
        },
        {
          "id": 56395,
          "name": "Waleed Ahmed Ali",
          "nationality": None
        },
        {
          "id": 56019,
          "name": "Norbert Hauata",
          "nationality": None
        },
        {
          "id": 56020,
          "name": "Bertrand Brial",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        },
        {
          "id": 9559,
          "name": "Sander van Roekel",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 55724,
          "name": "Mark Geiger",
          "nationality": None
        }
      ]
    },
    {
      "id": 200044,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-24T15:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group H",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "DRAW",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 766,
        "name": "Japan"
      },
      "awayTeam": {
        "id": 804,
        "name": "Senegal"
      },
      "referees": [
        {
          "id": 10988,
          "name": "Gianluca Rocchi",
          "nationality": None
        },
        {
          "id": 11056,
          "name": "Elenito Di Liberatore",
          "nationality": None
        },
        {
          "id": 11058,
          "name": "Mauro Tonolini",
          "nationality": None
        },
        {
          "id": 56026,
          "name": "Abdelrahman Al Jassim",
          "nationality": None
        },
        {
          "id": 56027,
          "name": "Taleb Salem Al Marri",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 49487,
          "name": "Hernán Maidana",
          "nationality": None
        },
        {
          "id": 15713,
          "name": "Tiago Martins",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        }
      ]
    },
    {
      "id": 200045,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-24T18:00:00Z",
      "status": "FINISHED",
      "matchday": 2,
      "stage": "GROUP_STAGE",
      "group": "Group H",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 3
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 794,
        "name": "Poland"
      },
      "awayTeam": {
        "id": 818,
        "name": "Colombia"
      },
      "referees": [
        {
          "id": 56408,
          "name": "César Arturo Ramos",
          "nationality": None
        },
        {
          "id": 56008,
          "name": "Marvin Torrentera",
          "nationality": None
        },
        {
          "id": 56009,
          "name": "Miguel Ángel Hernández",
          "nationality": None
        },
        {
          "id": 23895,
          "name": "Julio Bascuñán",
          "nationality": None
        },
        {
          "id": 55659,
          "name": "Christian Schiemann",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 82,
          "name": "Roberto Díaz",
          "nationality": None
        },
        {
          "id": 18127,
          "name": "Gery Vargas",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        }
      ]
    },
    {
      "id": 200004,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-25T14:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group A",
      "lastUpdated": "2019-01-11T17:30:00Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 801,
        "name": "Saudi Arabia"
      },
      "awayTeam": {
        "id": 825,
        "name": "Egypt"
      },
      "referees": [
        {
          "id": 23062,
          "name": "Wilmar Roldán",
          "nationality": None
        },
        {
          "id": 23046,
          "name": "Alexander Guzmán",
          "nationality": None
        },
        {
          "id": 22961,
          "name": "Christian de la Cruz",
          "nationality": None
        },
        {
          "id": 56024,
          "name": "Ricardo Montero",
          "nationality": None
        },
        {
          "id": 56063,
          "name": "Hiroshi Yamauchi",
          "nationality": None
        },
        {
          "id": 43540,
          "name": "Artur Soares Dias",
          "nationality": None
        },
        {
          "id": 15713,
          "name": "Tiago Martins",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 11412,
          "name": "Wilton Pereira Sampaio",
          "nationality": None
        }
      ]
    },
    {
      "id": 200005,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-25T14:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group A",
      "lastUpdated": "2019-01-11T17:30:00Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 3,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 2,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 758,
        "name": "Uruguay"
      },
      "awayTeam": {
        "id": 808,
        "name": "Russia"
      },
      "referees": [
        {
          "id": 55961,
          "name": "Malang Diédhiou",
          "nationality": None
        },
        {
          "id": 55962,
          "name": "Djibril Camara",
          "nationality": None
        },
        {
          "id": 55963,
          "name": "El Hadji Malick Samba",
          "nationality": None
        },
        {
          "id": 55964,
          "name": "Bamlak Tessema",
          "nationality": None
        },
        {
          "id": 56084,
          "name": "Hasan Mohamed Al Mahri",
          "nationality": None
        },
        {
          "id": 9374,
          "name": "Clément Turpin",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 43884,
          "name": "Cyril Gringore",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        }
      ]
    },
    {
      "id": 200010,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-25T18:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group B",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "DRAW",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 840,
        "name": "Iran"
      },
      "awayTeam": {
        "id": 765,
        "name": "Portugal"
      },
      "referees": [
        {
          "id": 56073,
          "name": "Enrique Cáceres",
          "nationality": None
        },
        {
          "id": 56074,
          "name": "Eduardo Cardozo",
          "nationality": None
        },
        {
          "id": 56075,
          "name": "Juan Zorrilla",
          "nationality": None
        },
        {
          "id": 55663,
          "name": "Mehdi Abid Charef",
          "nationality": None
        },
        {
          "id": 55664,
          "name": "Anouar Hmila",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 18127,
          "name": "Gery Vargas",
          "nationality": None
        },
        {
          "id": 49487,
          "name": "Hernán Maidana",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        }
      ]
    },
    {
      "id": 200011,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-25T18:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group B",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "DRAW",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 760,
        "name": "Spain"
      },
      "awayTeam": {
        "id": 815,
        "name": "Morocco"
      },
      "referees": [
        {
          "id": 56184,
          "name": "Ravshan Irmatov",
          "nationality": None
        },
        {
          "id": 56185,
          "name": "Abdukhamidullo Rasulov",
          "nationality": None
        },
        {
          "id": 56186,
          "name": "Jakhongir Saidov",
          "nationality": None
        },
        {
          "id": 55973,
          "name": "Mohammed Abdulla Hassan",
          "nationality": None
        },
        {
          "id": 55974,
          "name": "Mohamed Ahmed Al Hammadi",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 43876,
          "name": "Mark Borsch",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        }
      ]
    },
    {
      "id": 200016,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-26T14:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group C",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 779,
        "name": "Australia"
      },
      "awayTeam": {
        "id": 832,
        "name": "Peru"
      },
      "referees": [
        {
          "id": 43938,
          "name": "Sergey Karasev",
          "nationality": None
        },
        {
          "id": 43939,
          "name": "Anton Averianov",
          "nationality": None
        },
        {
          "id": 43940,
          "name": "Tikhon Kalugin",
          "nationality": None
        },
        {
          "id": 49525,
          "name": "Ryūji Satō",
          "nationality": None
        },
        {
          "id": 55658,
          "name": "Toru Sagara",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        },
        {
          "id": 55667,
          "name": "Jair Marrufo",
          "nationality": None
        },
        {
          "id": 43876,
          "name": "Mark Borsch",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        }
      ]
    },
    {
      "id": 200017,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-26T14:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group C",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "DRAW",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 782,
        "name": "Denmark"
      },
      "awayTeam": {
        "id": 773,
        "name": "France"
      },
      "referees": [
        {
          "id": 49488,
          "name": "Sandro Ricci",
          "nationality": None
        },
        {
          "id": 11448,
          "name": "Emerson Augusto De Carvalho",
          "nationality": None
        },
        {
          "id": 11468,
          "name": "Marcelo Carvalho Van Gasse",
          "nationality": None
        },
        {
          "id": 10988,
          "name": "Gianluca Rocchi",
          "nationality": None
        },
        {
          "id": 11058,
          "name": "Mauro Tonolini",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 11412,
          "name": "Wilton Pereira Sampaio",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 15713,
          "name": "Tiago Martins",
          "nationality": None
        }
      ]
    },
    {
      "id": 200022,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-26T18:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group D",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 776,
        "name": "Nigeria"
      },
      "awayTeam": {
        "id": 762,
        "name": "Argentina"
      },
      "referees": [
        {
          "id": 30687,
          "name": "Cüneyt Çakιr",
          "nationality": None
        },
        {
          "id": 30688,
          "name": "Bahattin Duran",
          "nationality": None
        },
        {
          "id": 30689,
          "name": "Tarık Ongun",
          "nationality": None
        },
        {
          "id": 9558,
          "name": "Björn Kuipers",
          "nationality": None
        },
        {
          "id": 9559,
          "name": "Sander van Roekel",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 43889,
          "name": "Paweł Sokolnicki",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        }
      ]
    },
    {
      "id": 200023,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-26T18:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group D",
      "lastUpdated": "2019-01-11T17:30:01Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 1066,
        "name": "Iceland"
      },
      "awayTeam": {
        "id": 799,
        "name": "Croatia"
      },
      "referees": [
        {
          "id": 43869,
          "name": "Antonio Matéu",
          "nationality": None
        },
        {
          "id": 134,
          "name": "Pau Cebrián",
          "nationality": None
        },
        {
          "id": 82,
          "name": "Roberto Díaz",
          "nationality": None
        },
        {
          "id": 56010,
          "name": "John Pitti",
          "nationality": None
        },
        {
          "id": 56011,
          "name": "Gabriel Victoria",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        },
        {
          "id": 18127,
          "name": "Gery Vargas",
          "nationality": None
        },
        {
          "id": 11056,
          "name": "Elenito Di Liberatore",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        }
      ]
    },
    {
      "id": 200034,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-27T14:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group F",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 772,
        "name": "Korea Republic"
      },
      "awayTeam": {
        "id": 759,
        "name": "Germany"
      },
      "referees": [
        {
          "id": 55724,
          "name": "Mark Geiger",
          "nationality": None
        },
        {
          "id": 55725,
          "name": "Joe Fletcher",
          "nationality": None
        },
        {
          "id": 56076,
          "name": "Frank Anderson",
          "nationality": None
        },
        {
          "id": 23895,
          "name": "Julio Bascuñán",
          "nationality": None
        },
        {
          "id": 55659,
          "name": "Christian Schiemann",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        },
        {
          "id": 56025,
          "name": "Corey Rockwell",
          "nationality": None
        },
        {
          "id": 15713,
          "name": "Tiago Martins",
          "nationality": None
        },
        {
          "id": 43540,
          "name": "Artur Soares Dias",
          "nationality": None
        }
      ]
    },
    {
      "id": 200035,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-27T14:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group F",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 3
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 769,
        "name": "Mexico"
      },
      "awayTeam": {
        "id": 792,
        "name": "Sweden"
      },
      "referees": [
        {
          "id": 49485,
          "name": "Néstor Pitana",
          "nationality": None
        },
        {
          "id": 49487,
          "name": "Hernán Maidana",
          "nationality": None
        },
        {
          "id": 49486,
          "name": "Juan Pablo Belatti",
          "nationality": None
        },
        {
          "id": 29474,
          "name": "Andrés Cunha",
          "nationality": None
        },
        {
          "id": 28832,
          "name": "Mauricio Espinosa",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 18127,
          "name": "Gery Vargas",
          "nationality": None
        },
        {
          "id": 11412,
          "name": "Wilton Pereira Sampaio",
          "nationality": None
        }
      ]
    },
    {
      "id": 200028,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-27T18:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group E",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 780,
        "name": "Serbia"
      },
      "awayTeam": {
        "id": 764,
        "name": "Brazil"
      },
      "referees": [
        {
          "id": 55970,
          "name": "Alireza Faghani",
          "nationality": None
        },
        {
          "id": 55971,
          "name": "Reza Sokhandan",
          "nationality": None
        },
        {
          "id": 55972,
          "name": "Mohammadreza Mansouri",
          "nationality": None
        },
        {
          "id": 56071,
          "name": "Nawaf Shukralla",
          "nationality": None
        },
        {
          "id": 55664,
          "name": "Anouar Hmila",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 43889,
          "name": "Paweł Sokolnicki",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        }
      ]
    },
    {
      "id": 200029,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-27T18:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group E",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "DRAW",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 788,
        "name": "Switzerland"
      },
      "awayTeam": {
        "id": 793,
        "name": "Costa Rica"
      },
      "referees": [
        {
          "id": 9374,
          "name": "Clément Turpin",
          "nationality": None
        },
        {
          "id": 43883,
          "name": "Nicolas Danos",
          "nationality": None
        },
        {
          "id": 43884,
          "name": "Cyril Gringore",
          "nationality": None
        },
        {
          "id": 56019,
          "name": "Norbert Hauata",
          "nationality": None
        },
        {
          "id": 56020,
          "name": "Bertrand Brial",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 43876,
          "name": "Mark Borsch",
          "nationality": None
        },
        {
          "id": 43888,
          "name": "Szymon Marciniak",
          "nationality": None
        }
      ]
    },
    {
      "id": 200046,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-28T14:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group H",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 766,
        "name": "Japan"
      },
      "awayTeam": {
        "id": 794,
        "name": "Poland"
      },
      "referees": [
        {
          "id": 56021,
          "name": "Janny Sikazwe",
          "nationality": None
        },
        {
          "id": 56022,
          "name": "Jerson dos Santos",
          "nationality": None
        },
        {
          "id": 33897,
          "name": "Zakhele Siwela",
          "nationality": None
        },
        {
          "id": 56024,
          "name": "Ricardo Montero",
          "nationality": None
        },
        {
          "id": 56018,
          "name": "Juan Carlos Mora",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        },
        {
          "id": 18127,
          "name": "Gery Vargas",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        }
      ]
    },
    {
      "id": 200047,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-28T14:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group H",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 804,
        "name": "Senegal"
      },
      "awayTeam": {
        "id": 818,
        "name": "Colombia"
      },
      "referees": [
        {
          "id": 9371,
          "name": "Milorad Mažić",
          "nationality": None
        },
        {
          "id": 9372,
          "name": "Milovan Ristić",
          "nationality": None
        },
        {
          "id": 56231,
          "name": "Dalibor Đurđević",
          "nationality": None
        },
        {
          "id": 55964,
          "name": "Bamlak Tessema",
          "nationality": None
        },
        {
          "id": 56084,
          "name": "Hasan Mohamed Al Mahri",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 11056,
          "name": "Elenito Di Liberatore",
          "nationality": None
        },
        {
          "id": 10988,
          "name": "Gianluca Rocchi",
          "nationality": None
        }
      ]
    },
    {
      "id": 200040,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-28T18:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group G",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 770,
        "name": "England"
      },
      "awayTeam": {
        "id": 805,
        "name": "Belgium"
      },
      "referees": [
        {
          "id": 9346,
          "name": "Damir Skomina",
          "nationality": None
        },
        {
          "id": 9347,
          "name": "Jure Praprotnik",
          "nationality": None
        },
        {
          "id": 9348,
          "name": "Robert Vukan",
          "nationality": None
        },
        {
          "id": 55973,
          "name": "Mohammed Abdulla Hassan",
          "nationality": None
        },
        {
          "id": 55974,
          "name": "Mohamed Ahmed Al Hammadi",
          "nationality": None
        },
        {
          "id": 43540,
          "name": "Artur Soares Dias",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 82,
          "name": "Roberto Díaz",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        }
      ]
    },
    {
      "id": 200041,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-28T18:00:00Z",
      "status": "FINISHED",
      "matchday": 3,
      "stage": "GROUP_STAGE",
      "group": "Group G",
      "lastUpdated": "2019-01-11T17:30:02Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 1836,
        "name": "Panama"
      },
      "awayTeam": {
        "id": 802,
        "name": "Tunisia"
      },
      "referees": [
        {
          "id": 56071,
          "name": "Nawaf Shukralla",
          "nationality": None
        },
        {
          "id": 56072,
          "name": "Yaser Tulefat",
          "nationality": None
        },
        {
          "id": 56027,
          "name": "Taleb Salem Al Marri",
          "nationality": None
        },
        {
          "id": 55663,
          "name": "Mehdi Abid Charef",
          "nationality": None
        },
        {
          "id": 56063,
          "name": "Hiroshi Yamauchi",
          "nationality": None
        },
        {
          "id": 15713,
          "name": "Tiago Martins",
          "nationality": None
        },
        {
          "id": 56026,
          "name": "Abdelrahman Al Jassim",
          "nationality": None
        },
        {
          "id": 56008,
          "name": "Marvin Torrentera",
          "nationality": None
        },
        {
          "id": 49488,
          "name": "Sandro Ricci",
          "nationality": None
        }
      ]
    },
    {
      "id": 200048,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-30T14:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "ROUND_OF_16",
      "group": "Round of 16",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 4,
          "awayTeam": 3
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 773,
        "name": "France"
      },
      "awayTeam": {
        "id": 762,
        "name": "Argentina"
      },
      "referees": [
        {
          "id": 55970,
          "name": "Alireza Faghani",
          "nationality": None
        },
        {
          "id": 55971,
          "name": "Reza Sokhandan",
          "nationality": None
        },
        {
          "id": 55972,
          "name": "Mohammadreza Mansouri",
          "nationality": None
        },
        {
          "id": 23895,
          "name": "Julio Bascuñán",
          "nationality": None
        },
        {
          "id": 55659,
          "name": "Christian Schiemann",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        }
      ]
    },
    {
      "id": 200049,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-06-30T18:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "ROUND_OF_16",
      "group": "Round of 16",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 758,
        "name": "Uruguay"
      },
      "awayTeam": {
        "id": 765,
        "name": "Portugal"
      },
      "referees": [
        {
          "id": 56408,
          "name": "César Arturo Ramos",
          "nationality": None
        },
        {
          "id": 56008,
          "name": "Marvin Torrentera",
          "nationality": None
        },
        {
          "id": 56009,
          "name": "Miguel Ángel Hernández",
          "nationality": None
        },
        {
          "id": 55667,
          "name": "Jair Marrufo",
          "nationality": None
        },
        {
          "id": 56025,
          "name": "Corey Rockwell",
          "nationality": None
        },
        {
          "id": 55724,
          "name": "Mark Geiger",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 55725,
          "name": "Joe Fletcher",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        }
      ]
    },
    {
      "id": 200050,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-01T14:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "ROUND_OF_16",
      "group": "Round of 16",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "PENALTY_SHOOTOUT",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "penalties": {
          "homeTeam": 3,
          "awayTeam": 4
        }
      },
      "homeTeam": {
        "id": 760,
        "name": "Spain"
      },
      "awayTeam": {
        "id": 808,
        "name": "Russia"
      },
      "referees": [
        {
          "id": 9558,
          "name": "Björn Kuipers",
          "nationality": None
        },
        {
          "id": 9559,
          "name": "Sander van Roekel",
          "nationality": None
        },
        {
          "id": 9560,
          "name": "Erwin Zeinstra",
          "nationality": None
        },
        {
          "id": 9374,
          "name": "Clément Turpin",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        },
        {
          "id": 43876,
          "name": "Mark Borsch",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        },
        {
          "id": 43883,
          "name": "Nicolas Danos",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        }
      ]
    },
    {
      "id": 200051,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-01T18:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "ROUND_OF_16",
      "group": "Round of 16",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "PENALTY_SHOOTOUT",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "penalties": {
          "homeTeam": 3,
          "awayTeam": 2
        }
      },
      "homeTeam": {
        "id": 799,
        "name": "Croatia"
      },
      "awayTeam": {
        "id": 782,
        "name": "Denmark"
      },
      "referees": [
        {
          "id": 49485,
          "name": "Néstor Pitana",
          "nationality": None
        },
        {
          "id": 49487,
          "name": "Hernán Maidana",
          "nationality": None
        },
        {
          "id": 49486,
          "name": "Juan Pablo Belatti",
          "nationality": None
        },
        {
          "id": 56073,
          "name": "Enrique Cáceres",
          "nationality": None
        },
        {
          "id": 56074,
          "name": "Eduardo Cardozo",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 18127,
          "name": "Gery Vargas",
          "nationality": None
        },
        {
          "id": 82,
          "name": "Roberto Díaz",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        }
      ]
    },
    {
      "id": 200052,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-02T14:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "ROUND_OF_16",
      "group": "Round of 16",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 764,
        "name": "Brazil"
      },
      "awayTeam": {
        "id": 769,
        "name": "Mexico"
      },
      "referees": [
        {
          "id": 10988,
          "name": "Gianluca Rocchi",
          "nationality": None
        },
        {
          "id": 11056,
          "name": "Elenito Di Liberatore",
          "nationality": None
        },
        {
          "id": 11058,
          "name": "Mauro Tonolini",
          "nationality": None
        },
        {
          "id": 43869,
          "name": "Antonio Matéu",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 134,
          "name": "Pau Cebrián",
          "nationality": None
        }
      ]
    },
    {
      "id": 200053,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-02T18:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "ROUND_OF_16",
      "group": "Round of 16",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 3,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 805,
        "name": "Belgium"
      },
      "awayTeam": {
        "id": 766,
        "name": "Japan"
      },
      "referees": [
        {
          "id": 55961,
          "name": "Malang Diédhiou",
          "nationality": None
        },
        {
          "id": 55962,
          "name": "Djibril Camara",
          "nationality": None
        },
        {
          "id": 55963,
          "name": "El Hadji Malick Samba",
          "nationality": None
        },
        {
          "id": 55660,
          "name": "Bakary Papa Gassama",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        },
        {
          "id": 43876,
          "name": "Mark Borsch",
          "nationality": None
        },
        {
          "id": 9374,
          "name": "Clément Turpin",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        },
        {
          "id": 55661,
          "name": "Jean-Claude Birumushahu",
          "nationality": None
        }
      ]
    },
    {
      "id": 200054,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-03T14:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "ROUND_OF_16",
      "group": "Round of 16",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 792,
        "name": "Sweden"
      },
      "awayTeam": {
        "id": 788,
        "name": "Switzerland"
      },
      "referees": [
        {
          "id": 9346,
          "name": "Damir Skomina",
          "nationality": None
        },
        {
          "id": 9347,
          "name": "Jure Praprotnik",
          "nationality": None
        },
        {
          "id": 9348,
          "name": "Robert Vukan",
          "nationality": None
        },
        {
          "id": 56071,
          "name": "Nawaf Shukralla",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        },
        {
          "id": 82,
          "name": "Roberto Díaz",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 56072,
          "name": "Yaser Tulefat",
          "nationality": None
        }
      ]
    },
    {
      "id": 200055,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-03T18:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "ROUND_OF_16",
      "group": "Round of 16",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "PENALTY_SHOOTOUT",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "penalties": {
          "homeTeam": 3,
          "awayTeam": 4
        }
      },
      "homeTeam": {
        "id": 818,
        "name": "Colombia"
      },
      "awayTeam": {
        "id": 770,
        "name": "England"
      },
      "referees": [
        {
          "id": 55724,
          "name": "Mark Geiger",
          "nationality": None
        },
        {
          "id": 55725,
          "name": "Joe Fletcher",
          "nationality": None
        },
        {
          "id": 56076,
          "name": "Frank Anderson",
          "nationality": None
        },
        {
          "id": 56176,
          "name": "Matt Conger",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 56177,
          "name": "Tevita Makasini",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        }
      ]
    },
    {
      "id": 200056,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-06T14:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "QUARTER_FINALS",
      "group": "Quarter-finals",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 758,
        "name": "Uruguay"
      },
      "awayTeam": {
        "id": 773,
        "name": "France"
      },
      "referees": [
        {
          "id": 49485,
          "name": "Néstor Pitana",
          "nationality": None
        },
        {
          "id": 49487,
          "name": "Hernán Maidana",
          "nationality": None
        },
        {
          "id": 49486,
          "name": "Juan Pablo Belatti",
          "nationality": None
        },
        {
          "id": 55970,
          "name": "Alireza Faghani",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        },
        {
          "id": 55971,
          "name": "Reza Sokhandan",
          "nationality": None
        }
      ]
    },
    {
      "id": 200057,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-06T18:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "QUARTER_FINALS",
      "group": "Quarter-finals",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 2
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 764,
        "name": "Brazil"
      },
      "awayTeam": {
        "id": 805,
        "name": "Belgium"
      },
      "referees": [
        {
          "id": 9371,
          "name": "Milorad Mažić",
          "nationality": None
        },
        {
          "id": 9372,
          "name": "Milovan Ristić",
          "nationality": None
        },
        {
          "id": 56231,
          "name": "Dalibor Đurđević",
          "nationality": None
        },
        {
          "id": 55667,
          "name": "Jair Marrufo",
          "nationality": None
        },
        {
          "id": 11116,
          "name": "Daniele Orsato",
          "nationality": None
        },
        {
          "id": 43876,
          "name": "Mark Borsch",
          "nationality": None
        },
        {
          "id": 43854,
          "name": "Paweł Gil",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        },
        {
          "id": 56025,
          "name": "Corey Rockwell",
          "nationality": None
        }
      ]
    },
    {
      "id": 200058,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-07T14:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "QUARTER_FINALS",
      "group": "Quarter-finals",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 0,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 792,
        "name": "Sweden"
      },
      "awayTeam": {
        "id": 770,
        "name": "England"
      },
      "referees": [
        {
          "id": 9558,
          "name": "Björn Kuipers",
          "nationality": None
        },
        {
          "id": 9559,
          "name": "Sander van Roekel",
          "nationality": None
        },
        {
          "id": 9560,
          "name": "Erwin Zeinstra",
          "nationality": None
        },
        {
          "id": 43869,
          "name": "Antonio Matéu",
          "nationality": None
        },
        {
          "id": 134,
          "name": "Pau Cebrián",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        }
      ]
    },
    {
      "id": 200059,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-07T18:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "QUARTER_FINALS",
      "group": "Quarter-finals",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "AWAY_TEAM",
        "duration": "PENALTY_SHOOTOUT",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": 1,
          "awayTeam": 1
        },
        "penalties": {
          "homeTeam": 3,
          "awayTeam": 4
        }
      },
      "homeTeam": {
        "id": 808,
        "name": "Russia"
      },
      "awayTeam": {
        "id": 799,
        "name": "Croatia"
      },
      "referees": [
        {
          "id": 49488,
          "name": "Sandro Ricci",
          "nationality": None
        },
        {
          "id": 11448,
          "name": "Emerson Augusto De Carvalho",
          "nationality": None
        },
        {
          "id": 11468,
          "name": "Marcelo Carvalho Van Gasse",
          "nationality": None
        },
        {
          "id": 56021,
          "name": "Janny Sikazwe",
          "nationality": None
        },
        {
          "id": 56022,
          "name": "Jerson dos Santos",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 11412,
          "name": "Wilton Pereira Sampaio",
          "nationality": None
        },
        {
          "id": 82,
          "name": "Roberto Díaz",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        }
      ]
    },
    {
      "id": 200060,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-10T18:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "SEMI_FINALS",
      "group": "Semi-finals",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 773,
        "name": "France"
      },
      "awayTeam": {
        "id": 805,
        "name": "Belgium"
      },
      "referees": [
        {
          "id": 29474,
          "name": "Andrés Cunha",
          "nationality": None
        },
        {
          "id": 28776,
          "name": "Nicolás Tarán",
          "nationality": None
        },
        {
          "id": 28832,
          "name": "Mauricio Espinosa",
          "nationality": None
        },
        {
          "id": 56408,
          "name": "César Arturo Ramos",
          "nationality": None
        },
        {
          "id": 56008,
          "name": "Marvin Torrentera",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 82,
          "name": "Roberto Díaz",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        }
      ]
    },
    {
      "id": 200061,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-11T18:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "SEMI_FINALS",
      "group": "Semi-finals",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "EXTRA_TIME",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 1
        },
        "halfTime": {
          "homeTeam": 0,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 799,
        "name": "Croatia"
      },
      "awayTeam": {
        "id": 770,
        "name": "England"
      },
      "referees": [
        {
          "id": 30687,
          "name": "Cüneyt Çakιr",
          "nationality": None
        },
        {
          "id": 30688,
          "name": "Bahattin Duran",
          "nationality": None
        },
        {
          "id": 30689,
          "name": "Tarık Ongun",
          "nationality": None
        },
        {
          "id": 9558,
          "name": "Björn Kuipers",
          "nationality": None
        },
        {
          "id": 9559,
          "name": "Sander van Roekel",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 43878,
          "name": "Felix Zwayer",
          "nationality": None
        }
      ]
    },
    {
      "id": 200062,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-14T14:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "3RD_PLACE",
      "group": "3rd Place",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 2,
          "awayTeam": 0
        },
        "halfTime": {
          "homeTeam": 1,
          "awayTeam": 0
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 805,
        "name": "Belgium"
      },
      "awayTeam": {
        "id": 770,
        "name": "England"
      },
      "referees": [
        {
          "id": 55970,
          "name": "Alireza Faghani",
          "nationality": None
        },
        {
          "id": 55971,
          "name": "Reza Sokhandan",
          "nationality": None
        },
        {
          "id": 55972,
          "name": "Mohammadreza Mansouri",
          "nationality": None
        },
        {
          "id": 55961,
          "name": "Malang Diédhiou",
          "nationality": None
        },
        {
          "id": 55962,
          "name": "Djibril Camara",
          "nationality": None
        },
        {
          "id": 55724,
          "name": "Mark Geiger",
          "nationality": None
        },
        {
          "id": 15746,
          "name": "Bastian Dankert",
          "nationality": None
        },
        {
          "id": 55725,
          "name": "Joe Fletcher",
          "nationality": None
        },
        {
          "id": 11075,
          "name": "Paolo Valeri",
          "nationality": None
        }
      ]
    },
    {
      "id": 200063,
      "season": {
        "id": 1,
        "startDate": "2018-06-14",
        "endDate": "2018-07-15",
        "currentMatchday": 3
      },
      "utcDate": "2018-07-15T15:00:00Z",
      "status": "FINISHED",
      "matchday": None,
      "stage": "FINAL",
      "group": "Final",
      "lastUpdated": "2019-01-11T17:30:03Z",
      "score": {
        "winner": "HOME_TEAM",
        "duration": "REGULAR",
        "fullTime": {
          "homeTeam": 4,
          "awayTeam": 2
        },
        "halfTime": {
          "homeTeam": 2,
          "awayTeam": 1
        },
        "extraTime": {
          "homeTeam": None,
          "awayTeam": None
        },
        "penalties": {
          "homeTeam": None,
          "awayTeam": None
        }
      },
      "homeTeam": {
        "id": 773,
        "name": "France"
      },
      "awayTeam": {
        "id": 799,
        "name": "Croatia"
      },
      "referees": [
        {
          "id": 49485,
          "name": "Néstor Pitana",
          "nationality": None
        },
        {
          "id": 49487,
          "name": "Hernán Maidana",
          "nationality": None
        },
        {
          "id": 49486,
          "name": "Juan Pablo Belatti",
          "nationality": None
        },
        {
          "id": 9558,
          "name": "Björn Kuipers",
          "nationality": None
        },
        {
          "id": 9560,
          "name": "Erwin Zeinstra",
          "nationality": None
        },
        {
          "id": 11054,
          "name": "Massimiliano Irrati",
          "nationality": None
        },
        {
          "id": 49527,
          "name": "Mauro Vigliano",
          "nationality": None
        },
        {
          "id": 49528,
          "name": "Carlos Astroza",
          "nationality": None
        },
        {
          "id": 43899,
          "name": "Danny Makkelie",
          "nationality": None
        }
      ]
    }
  ]))






"""
6. In questo esercizio si deve verificare la presenza di alcune chiavi nel
   dizionario 'wc_dict'.

   SE la chiave presente stampare sulla console il messaggio "Il dizionario contiene la chiave!".
   ALTRIMENTI stampare il messaggio "Errore!!".

   Per verificare la presenza della chiave si deve utilizzare la parola chiave "in".

   6a. Verificare la presenza della chiave "matches".
   6b. Verificare la presenza della chiave "pippo".
"""
# scrivi qua il tuo codice
if "matches" in wc_dict:
    print('Il dizionario contiene la chiave "matches"!')
if "pippo" in wc_dict:
    print('Il dizionario contiene la chiave "pippo"!')
else:
    print('Errore il dizionario non contiene la chiave "pippo"!!')








"""
7. Dizionari dentro dizionari.
   Il dizionario 'wc_dict' contiene la chiave "competition". Il valore associato
   a questa chiave è a sua volta un dizionario.

   7a. Stampare sulla console il nome della competizione (ovvero "FIFA World Cup").
   7b. Stampare sulla console tutte le chiavi del dizionario associato alla chiave "competition".
   7c. Verificare la presenza della chiave "code" all'interno del dizionario associato alla chiave "competition".

"""
# scrivi qua il tuo codice
print(wc_dict["competition"]['name'])
print(wc_dict['competition'].keys())
if "code" in wc_dict['competition']:
    print('Il valore della chiave "competition", che è un dizionario, contiene la chiave "code"!')



"""
8. Valori "None".
   8a. Testare il valore associato alla chiave "flag". SE è None stampare sulla console
   "il campo flag è None" ALTRIMENTI stampare il valore associato alla chiave.
   8b. Testare il valore associato alla chiave "name". SE è None stampare sulla console
   "il campo name è None" ALTRIMENTI stampare il valore associato alla chiave.

"""
# scrivi qua il tuo codice
if wc_dict['competition']['flag']==None:
    print('Il campo flag è None')
else:
    print('Il campo flag è', wc_dict['competition']['flag'])

if wc_dict['competition']['name']==None:
    print('Il campo name è None')
else:
    print('Il campo name è', wc_dict['competition']['name'])







"""
9.  Dizionari dentro dizionari II.
    Dichiarare una variabile "val" e inizializzarla con il valore None.
    Verificare che la chiave "campo_A" sia presente, in tal caso verificare
    che il valore associato alla chiave "campo_A" non sia None. Se queste due
    condizioni si verificano, assegnare a "val" il valore associato alla chiave
    e stampare il valore di "val".
"""
# scrivi qua il tuo codice
chiave = input('Inserisci la chiave da cercare nel valore di "Competition" : ')
if chiave not in wc_dict['competition']:
    print('La chiave ', chiave, ' non è presente nel valore di "Competition"')
elif wc_dict['competition'][chiave] != None:
    print('La chiave ', chiave, ' è presente nel valore di "Competition" e il suo valore è', wc_dict['competition'][chiave])
else:
    print('La chiave ', chiave, ' è presente nel valore di "Competition" ma ha il valore None')
# oppure
chiave = input('Inserisci la chiave da cercare nel valore di "Competition" : ')
if chiave in wc_dict['competition'] and wc_dict['competition'][chiave] != None:
    print('La chiave ', chiave, ' è presente nel valore di "Competition" e il suo valore è', wc_dict['competition'][chiave])
else:
    print('La chiave ', chiave, ' non esiste o ha il valore None')
# oppure
val = None
chiave = input('Inserisci la chiave da cercare nel valore di "Competition" : ')
if chiave in wc_dict['competition'] and wc_dict['competition'][chiave] != None:
    val = wc_dict['competition'][chiave]
    print('La chiave ', chiave, ' è presente nel valore di "Competition" e il suo valore è', val)
else:
    print('La chiave ', chiave, ' non esiste o ha il valore ', val)


"""
10. Esercizio avanzato.
    10a. Trovare il numero delle partite che sono finite ai rigori.
    10b. Trovare il numero di goals che sono stati segnati (in tutte le partite).
"""
# scrivi qua il tuo codice

# 10a prima soluzione con diversi moccoli
rigori=[]
i=0
while i<len(wc_dict["matches"]):
    if wc_dict['matches'][i]['score']['duration']=="PENALTY_SHOOTOUT":
        rigori.append(wc_dict['matches'][i]['score']['duration'])
    i=i+1
print(rigori.count("PENALTY_SHOOTOUT"))
# 4


# 10b prima soluzione con diversi moccoli
goaltotali=[]
i=0
while i<len(wc_dict["matches"]):
    if wc_dict['matches'][i]['status']=="FINISHED":
        goaltotali.append(wc_dict['matches'][i]['score']['fullTime']['homeTeam'])
        goaltotali.append(wc_dict['matches'][i]['score']['fullTime']['awayTeam'])
    if wc_dict['matches'][i]['score']['duration']=="PENALTY_SHOOTOUT":
        goaltotali.append(wc_dict['matches'][i]['score']['penalties']['homeTeam'])
        goaltotali.append(wc_dict['matches'][i]['score']['penalties']['awayTeam'])
    i=i+1
print(sum(goaltotali))
# 195


# 10a seconda soluzione con un monte di moccoli
k=0
for b in wc_dict['matches']:
    if b["score"]['duration']=="PENALTY_SHOOTOUT":
        k=k+1
print(k)
# 4


# 10b seconda soluzione con un monte di moccoli
i=0
for a in wc_dict['matches']:
    m=a["score"]["fullTime"]["homeTeam"]
    n=a["score"]["fullTime"]["awayTeam"]
    i=i+m+n
# print(i)
k=0
for b in wc_dict['matches']:
    if b["score"]['duration']=="PENALTY_SHOOTOUT":
        v=b['score']['penalties']['homeTeam']
        z=b['score']['penalties']['awayTeam']
        k=k+v+z
# print(k)
w=i+k
print(w)
# 195


# 10b terza soluzione con un po' meno moccoli
i=0
for a in wc_dict['matches']:
    m=a["score"]["fullTime"]["homeTeam"]
    n=a["score"]["fullTime"]["awayTeam"]
    i=i+m+n
for b in wc_dict['matches']:
    if b["score"]['duration']=="PENALTY_SHOOTOUT":
        v=b['score']['penalties']['homeTeam']
        z=b['score']['penalties']['awayTeam']
        i=i+v+z
print(i)
# 195


# 10b quarta soluzione con un'esagerazione di moccoli e una notte insonne :-)
# ma poi torna!!!!
k=0
for b in wc_dict['matches']:
    if b["score"]['duration']=="PENALTY_SHOOTOUT":
        v=b['score']['penalties']['homeTeam']
        z=b['score']['penalties']['awayTeam']
        w=b["score"]["fullTime"]["homeTeam"]
        y=b["score"]["fullTime"]["awayTeam"]
        k=k+v+z+w+y
    else:
        m=b["score"]["fullTime"]["homeTeam"]
        n=b["score"]["fullTime"]["awayTeam"]
        k=k+m+n
print(k)
# 195


"""
Metodi built-in:

    keys()  --> https://www.w3schools.com/python/ref_dictionary_keys.asp
    len()   --> https://www.w3schools.com/python/ref_func_len.asp
    print() --> https://www.w3schools.com/python/ref_func_print.asp
    type()  --> https://www.w3schools.com/python/ref_func_type.asp
"""
